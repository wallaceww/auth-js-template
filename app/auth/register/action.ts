'use server';

import bcrypt from 'bcrypt';
import { prisma } from '@/lib/prisma';
import { TSignupSchema, signUpSchema } from '@/lib/schema';
import { generateVerificationToken } from '@/lib/util';
import { Email } from '@/lib/mail';

export async function registerUser(data: TSignupSchema) {
    const result = signUpSchema.safeParse(data);

    if (!result.success) {
        return { sucess: false, error: result.error.format() };
    }

    const duplicate = await prisma.user.findUnique({
        where: {
            email: data.email,
        }
    });

    if (duplicate) {
        return { success: false, error: { message: 'Duplicate email' }};
    }

    try {
        const hashedPassword = await bcrypt.hash(data.password, 10);
        await prisma.user.create({
            data: {
                name: data.name,
                email: data.email,
                password: hashedPassword,
            }
        });

        const { token } = await generateVerificationToken(data.email);

        const verificationUrl = `http://localhost:3000/auth/new-verification?token=${token}`;
        const verificationEmail = new Email(data.email, verificationUrl);
        await verificationEmail.sendVerfication();

        return { success: true, message: 'Confirmation email sent' };
    } catch (error) {
        console.log(error);
        return { sucess: false };
    }
}