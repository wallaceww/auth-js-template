'use client';

import { useState } from 'react';
import { logOut, login } from './auth/login/action';
import { useCurrentUser } from '@/hooks/use-current-user';
import Link from 'next/link';
import Modal from './components/Modal';
import LoginHookForm from './auth/login/page';

export default function Home() {
  const user = useCurrentUser();
  const [isOpen, setIsOpen] = useState(false);

  function toggleModal() {
    setIsOpen(prevState => !prevState);
  }

  return (
    <div>
      <h1>Hello world</h1>
      <p>
        {JSON.stringify(user)}
      </p>
      <form
        action={async () => {
          await login('google');
        }}
      >
        <button>Google</button>
      </form>
      <form
        action={async () => {
          await login('github');
        }}
      >
        <button>Github</button>
      </form>
      <form
        action={async () => {
          await login();
        }}
      >
        <button>Credentials</button>
      </form>
      <button onClick={toggleModal}>
        Open Modal
      </button>
      <Modal
        isOpen={isOpen}
        onClose={toggleModal}
      >
        <LoginHookForm />
      </Modal>
      <p>
        <Link href={'/settings'}>
          Settings
        </Link>
      </p>
      <form
        action={async () => {
          await logOut();
        }}
      >
        <button>Sign out</button>
      </form>
    </div>
  );
}
