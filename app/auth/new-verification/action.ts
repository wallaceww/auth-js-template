'use server';

import { getUserByEmail, getVerificationTokenByToken } from '@/lib/dbQuery';
import { prisma } from '@/lib/prisma';
import moment from 'moment';

export async function newVerification (token: string) {
    try {
        const existingToken = await getVerificationTokenByToken(token);

        if (!existingToken) {
            return { error: 'Token does not exist!' };
        }

        // TODO: set up cron job to delete expired tokens
        const hasExpired = moment(existingToken.expires).isBefore(moment());

        if (hasExpired) {
            return { error: 'Token has expired!' };
        }

        const existingUser = await getUserByEmail(existingToken.email);

        if (!existingUser) {
            return { error: 'User does not exist!' };
        }

        await prisma.user.update({
            where: { id: existingUser.id },
            data: {
                emailVerified: moment().toDate(),
            },
        });

        await prisma.verificationToken.delete({
            where: { id: existingToken.id },
        });

        return { success: 'Email verified' };
    } catch (err) {
        return { error: 'Something went wrong' };
    }
}