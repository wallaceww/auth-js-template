import NextAuth from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";
import bcrypt from "bcrypt";
import { authConfig } from "./auth.config";
import { PrismaAdapter } from "@auth/prisma-adapter";
import { prisma } from "./lib/prisma";
import { getAccountByUserId, getTwoFactorConfirmationByUserId, getUserById } from "./lib/dbQuery";
import moment from "moment";

export const {
  handlers: { GET, POST },
  auth,
  signIn,
  signOut,
} = NextAuth({
  adapter: PrismaAdapter(prisma),
  session: { strategy: "jwt" },
  trustHost: true,
  ...authConfig,
  providers: [
    ...authConfig.providers,
    CredentialsProvider({
      name: "Credentials",
      credentials: {
        email: {
          label: "email",
          type: "text",
          placeholder: "Your email",
        },
        password: { 
          label: "password",
          type: "password",
          placeholder: "Your password",
        },
      },
      async authorize(credentials) {
        try {
          const { email, password } = credentials;

          const foundUser = await prisma.user.findUnique({
            where: {
              email: email as string,
            },
          });

          if (!foundUser?.password) {
            return null;
          }

          if (foundUser) {
            const passwordMatch = await bcrypt.compare(
              password as string, foundUser.password
            );

            if (passwordMatch) {
              return {
                id: foundUser.id,
                name: foundUser.name,
                email: foundUser.email,
                role: foundUser.role,
              };
            }
          }
        } catch (error) {
          console.log("error", error);
        }
        return null;
      },
    }),
  ],
  events: {
    async linkAccount({ user }) {
      // triggered when an account in a given provider is linked to a user in our user database
      await prisma.user.update({
        where: { id: user.id },
        data: { emailVerified: moment().toDate() },
      });
    }
  },
  callbacks: {
    async signIn({ user, account }) {
      // Allow OAuth without email verification
      if (account?.provider !== 'credentials') {
        return true;
      }

      const existingUser = await getUserById(user.id!);

      if (!existingUser?.emailVerified) {
        return false;
      }

      if (existingUser?.isTwoFactorEnabled) {
        const twoFactorConfirmation = await getTwoFactorConfirmationByUserId(existingUser.id);
        if (!twoFactorConfirmation) {
          return false;
        }

        // Delete 2FA confirmation for next sign in
        await prisma.twoFactorConfirmation.delete({
          where: {
            id: twoFactorConfirmation.id,
          },
        });
      }

      return true;
    },
    async jwt({ token }) {
      console.log('jwt')
      if (!token.sub) return token;

      const userExist = await getUserById(token.sub);

      if (!userExist) return token;

      const accountExist = await getAccountByUserId(userExist.id);

      token.isOAuth = !!accountExist;
      token.name = userExist.name;
      token.email = userExist.email;
      token.role = userExist.role;
      token.isTwoFactorEnabled = userExist.isTwoFactorEnabled;

      console.log({ token });
      return token;
    },
    async session({ session, user, token }) {
      if (session.user) {
        session.user.id = token.sub;
        session.user.name = token.name;
        session.user.email = token.email;
        session.user.role = token.role;
        session.user.isTwoFactorEnabled = token.isTwoFactorEnabled;
        session.user.isOAuth = token.isOAuth;
      }
      console.log({
        sessionToken: token,
        session
      })
      return session;
    },
  },
});
