'use client';

import InputGroup from '@/app/components/InputGroup';
import { zodResolver } from '@hookform/resolvers/zod';
import Link from 'next/link';
import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { resetPassword, verifyResetPasswordToken } from './action';
import { TResetPasswordSchema, resetPasswordSchema } from '@/lib/schema';
import { useSearchParams } from 'next/navigation';

export default function ResetPasswordPage() {
    const {
        register,
        handleSubmit,
        formState: { isSubmitting, isValid, errors },
        reset,
    } = useForm<TResetPasswordSchema>({
        mode: 'onTouched',
        resolver: zodResolver(resetPasswordSchema),
    });

    const passwordValidation = {
        label: 'Password',
        type: 'password',
        register: register,
        errors: errors,
    };

    const confirmPasswordValidation = {
        label: 'Confirm Password',
        type: 'password',
        register: register,
        errors: errors,
    };

    const token = useSearchParams().get('token');

    useEffect(() => {
        if (!token) {
            setError('Missing token');
            return;
        }

        (async () => {
            const isTokenValid = await verifyResetPasswordToken(token);

            if (!isTokenValid) {
                setError('Token is invalid');
            }
        })();
    }, [token]);

    const [error, setError] = useState<string | undefined>('');
    const [success, setSuccess] = useState<string | undefined>('');

    const onSubmit = handleSubmit(async data => {
        setError('');
        setSuccess('');

        const result = await resetPassword(data, token);

        if (!result) {
            console.log('Something went wrong');
            return;
        }

        if (result.error) {
            setError(result.error);
            return;
        }

        setSuccess(result.success);

        reset();
    });

    return (
        <div className="min-h-full flex justify-center my-10">
            <div className="flex flex-col justify-center items-center w-1/2 py-10 shadow-md rounded bg-white">
                <div className="sm:w-full sm:max-w-sm">
                    <h2 className="text-center text-2xl font-bold text-gray-900">
                        {
                            error === 'Token is invalid' ? error : 'Reset Password'
                        }
                    </h2>
                </div>
                <div className="mt-10 sm:w-full sm:max-w-sm">
                    {
                        error === 'Token is invalid' ?

                            null

                            :

                            <form onSubmit={onSubmit} className="flex flex-col gap-3">
                                <div>
                                    <InputGroup<TResetPasswordSchema>
                                        name='password'
                                        {...passwordValidation}
                                    />
                                </div>
                                <div>
                                    <InputGroup<TResetPasswordSchema>
                                        name='confirmPassword'
                                        {...confirmPasswordValidation}
                                    />
                                </div>
                                <button
                                    disabled={!isValid || isSubmitting}
                                    type="submit"
                                    className="bg-indigo-600 mt-5 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm
                                        disabled:bg-slate-300
                                        hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600 rounded"
                                >
                                    {isSubmitting ? 'Submitting...' : 'Submit'}
                                </button>
                            </form>
                    }
                    {
                        success && <p>{success}</p>
                    }
                    {
                        error && <p>{error}</p>
                    }
                    <div className="mt-5">
                        <p className="text-center">
                            <Link href='/auth/login'
                                className="underline"
                            >
                                Back to login
                            </Link>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
}