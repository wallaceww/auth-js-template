'use client';

import Link from 'next/link';
import { useSearchParams } from 'next/navigation';
import { useCallback, useEffect, useState } from 'react';
import { newVerification } from './action';

export default function NewVerificationPage() {
    const [error, setError] = useState<string | undefined>();
    const [success, setSuccess] = useState<string | undefined>();

    const token = useSearchParams().get('token');

    const onSubmit = useCallback(() => {
        if (!token) {
            setError('Missing token!');
            return;
        }

        newVerification(token)
            .then((data) => {
                setSuccess(data.success);
                setError(data.error);
            })
            .catch(() => {
                setError('Something went wrong');
            });
    }, [token]);

    useEffect(() => {
        onSubmit();
    }, [onSubmit]);


    return (
        <div className="min-h-full flex justify-center my-10">
            <div className="flex flex-col justify-center items-center w-1/2 py-10 shadow-md rounded bg-white">
                <div className="sm:w-full sm:max-w-sm">
                    <h2 className="text-center text-2xl font-bold text-gray-900">
                        Confirming your email
                    </h2>
                </div>
                <div className="mt-10 sm:w-full sm:max-w-sm">
                    {
                        !success && !error &&
                        <p>Loading...</p>
                    }
                    {
                        success && <p>{success}</p>
                    }
                    {
                        error && <p>{error}</p>
                    }
                    <div className="mt-5">
                        <p className="text-center">
                            {
                                'Back to '
                            }
                            <Link href={'/auth/login'}
                                className="underline"
                            >
                                login
                            </Link>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
}
