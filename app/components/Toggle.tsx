import { Switch } from '@headlessui/react';

export default function Toggle({
    label, enabled, onChange
}: Readonly<{ label: string, enabled: boolean, onChange: any }>) {
    return (
        <Switch.Group>
            <div className="flex items-center justify-between">
                <Switch.Label className='mr-4'>
                    {label}
                </Switch.Label>
                <Switch
                    checked={enabled}
                    onChange={onChange}
                    className={`${enabled ? 'bg-blue-600' : 'bg-gray-200'
                        } relative inline-flex h-6 w-11 items-center rounded-full transition-colors focus:outline-none`}
                >
                    <span className="sr-only">Use setting</span>
                    <span
                        aria-hidden='true'
                        className={`${enabled ? 'translate-x-6' : 'translate-x-1'
                            } inline-block h-4 w-4 transform rounded-full bg-white transition-transform`}
                    />
                </Switch>
            </div>
        </Switch.Group>
    );
}