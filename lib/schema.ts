import { z } from 'zod';

export const loginSchema = z.object({
    email: z.string(),
    password: z.string(),
    code: z.string().optional(),
});

export type TLoginSchema = z.infer<typeof loginSchema>;

export const signUpSchema = z.object({
    name: z.string().min(3, 'Name at least 3 characters'),
    email: z.string().email(),
    password: z.string().min(6, 'Password at least 6 characters'),
    confirmPassword: z.string(),
}).refine(
    (data) => data.password === data.confirmPassword, {
        message: 'Passwords must match',
        path: ['password', 'confirmPassword'],
    }
);

export type TSignupSchema = z.infer<typeof signUpSchema>;

export const forgetPasswordSchema = z.object({
    email: z.string().email(),
});

export type TForgetPasswordSchema = z.infer<typeof forgetPasswordSchema>;

export const resetPasswordSchema = z.object({
    password: z.string().min(6, 'Password at least 6 characters'),
    confirmPassword: z.string(),
}).refine(
    (data) => data.password === data.confirmPassword, {
        message: 'Passwords must match',
        path: ['confirmPassword'],
    }
);

export type TResetPasswordSchema = z.infer<typeof resetPasswordSchema>;

export const settingsSchema = z.object({
    name: z.string().min(1, { message: 'Name is required' }),
    email: z.string().email().optional(),
    isTwoFactorEnabled: z.boolean().optional(),
    role: z.enum(['User', 'Admin']),
    password: z.string().min(1, { message: 'Password is required' }).optional(),
    newPassword: z.string().min(6).optional(),
}).refine(data => {
    if (data.password && !data.newPassword) {
        return false;
    }
    return true;
}, {
    message: 'New password is required',
    path: ['newPassword']
}).refine(data => {
    if (!data.password && data.newPassword) {
        return false;
    }
    return true;
}, {
    message: 'Password is required',
    path: ['password']
}).refine(data => {
    if (data.password &&
        data.newPassword &&
        data.password === data.newPassword) {
        return false;
    }
    return true;
}, {
    message: 'New password should be different from the old password',
    path: ['password', 'newPassword']
});

export type TSettingsSchema = z.infer<typeof settingsSchema>;
