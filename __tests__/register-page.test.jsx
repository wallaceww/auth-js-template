import RegisterHookForm from "@/app/auth/register/page"
import { render, screen } from "@testing-library/react"

describe('Register Page', () => {
    it('renders a heading', () => {
        render(<RegisterHookForm />);

        const heading = screen.getByRole('heading', { level: 2 });
 
        expect(heading).toBeInTheDocument();
    })
})
