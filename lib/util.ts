import moment from 'moment';
import { v4 as uuid } from 'uuid';
import { randomInt } from 'crypto';

import { getPasswordResetTokenByEmail, getTwoFactorTokenByEmail, getVerificationTokenByEmail } from './dbQuery';
import { prisma } from './prisma';

export async function generateTwoFactorToken(email: string) {
    const token = randomInt(100_000, 1_000_000).toString();
    const expires = moment().add(1, 'h').toDate();

    const existingToken = await getTwoFactorTokenByEmail(email);

    if (existingToken) {
        await prisma.twoFactorToken.delete({
            where: {
                id: existingToken.id,
            },
        });
    }

    return await prisma.twoFactorToken.create({
        data: {
            email, token, expires
        },
    });
}

export async function generatePasswordResetToken(email: string) {
    const token = uuid();
    const expires = moment().add(1, 'h').toDate();

    const existingToken = await getPasswordResetTokenByEmail(email);

    if (existingToken) {
        await prisma.passwordResetToken.delete({
            where: {
                id: existingToken.id,
            },
        });
    }

    return await prisma.passwordResetToken.create({
        data: {
            email, token, expires
        },
    });
}

export async function generateVerificationToken(email: string) {
    const token = uuid();
    const expires = moment().add(1, 'h').toDate();

    const existingToken = await getVerificationTokenByEmail(email);

    if (existingToken) {
        await prisma.verificationToken.delete({
            where: {
                id: existingToken.id,
            },
        });
    }

    return await prisma.verificationToken.create({
        data: {
            email, token, expires
        },
    });
}