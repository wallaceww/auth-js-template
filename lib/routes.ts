export const publicRoutes = [
    '/'
];

export const authRoutes = [
    '/auth/login',
    '/auth/register',
    '/auth/forgot-password'
];

/**
 * The prefix for API authentication routes.
 * Routes that start with this prefix are used for
 authentication purposes
 */
export const apiRoutePrefix = '/api/auth';

export const DEFAULT_LOGIN_REDIRECT = '/settings';
