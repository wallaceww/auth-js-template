import "next-auth";
import { type AdapterUser } from "@auth/core/adapters"

export type ExtendedUser = DefaultSession["user"] & {
    role: string;
    isTwoFactorEnabled: boolean;
    isOAuth: boolean;
}

declare module "next-auth" {
    interface Session {
        user: ExtendedUser;
    }
}

declare module "next-auth/jwt" {
    interface JWT {
        role: string;
        isTwoFactorEnabled: boolean;
        isOAuth: boolean;
    }
}
