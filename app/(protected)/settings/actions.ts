'use server';

import bcrypt from 'bcrypt';
import { auth } from '@/auth';
import { getUserByEmail, getUserById } from '@/lib/dbQuery';
import { Email } from '@/lib/mail';
import { prisma } from '@/lib/prisma';
import { TSettingsSchema, settingsSchema } from '@/lib/schema';
import { generateVerificationToken } from '@/lib/util';

export async function updateParticulars(data: TSettingsSchema) {
    const parseData = settingsSchema.safeParse(data);

    if (!parseData.success) {
        return { typeError: parseData.error.format() };
    }

    const session = await auth();
    const userExist = await getUserById(session?.user.id);

    if (!session || !userExist) {
        return { error: 'Unauthorized' };
    }

    if (session.user.isOAuth) {
        data.email = undefined;
        data.password = undefined;
        data.newPassword = undefined;
        data.isTwoFactorEnabled = undefined;
    }

    if (data.email && data.email !== session.user.email) {
        const existingUser = await getUserByEmail(data.email);

        if(existingUser) {
            return { error: 'Email already in use' };
        }

        const { token } = await generateVerificationToken(data.email);
        const verificationUrl = `http://localhost:3000/auth/new-verification?token=${token}`;
        const verificationEmail = new Email(data.email, verificationUrl);
        await prisma.user.update({
            where: { id: userExist.id },
            data: { emailVerified: null },
        });
        await verificationEmail.sendVerfication();
    }

    if (data.password && data.newPassword) {
        const passwordMatch = await bcrypt.compare(data.password, (userExist.password) as string);

        if (!passwordMatch) {
            return { error: 'Incorrect password' };
        }

        const hashedPassword = await bcrypt.hash(data.newPassword, 10);
        data.password = hashedPassword;
        data.newPassword = undefined;
    }

    await prisma.user.update({
        where: { id: userExist.id },
        data: { ...data }
    });

    return { success: 'Settings updated' };
}
