'use client';

import InputGroup from '@/app/components/InputGroup';
import { TSettingsSchema, settingsSchema } from '@/lib/schema';
import { zodResolver } from '@hookform/resolvers/zod';
import { Controller, useForm } from 'react-hook-form';
import { updateParticulars } from './actions';
import { useSession } from 'next-auth/react';
import { Fragment, useEffect, useState } from 'react';
import { useCurrentUser } from '@/hooks/use-current-user';
import SelectDropdown from '@/app/components/SelectDropdown';
import Toggle from '@/app/components/Toggle';

export default function SettingsPage() {
    const [isClient, setIsClient] = useState(false);
    const [error, setError] = useState<string | undefined>();
    const [success, setSuccess] = useState<string | undefined>();

    useEffect(() => {
        setIsClient(true);
    }, []);

    const user = useCurrentUser();
    const { update } = useSession();
    const {
        control,
        register,
        getValues,
        handleSubmit,
        watch,
        formState: { isSubmitting, isValid, errors },
        reset,
        resetField,
    } = useForm<TSettingsSchema>({
        mode: 'all',
        resolver: zodResolver(settingsSchema),
        defaultValues: {
            name: user?.name || undefined,
            email: user?.email || undefined,
            isTwoFactorEnabled: user?.isTwoFactorEnabled || false,
            role: user?.role || 'User',
            password: undefined,
            newPassword: undefined,
        }
    });

    const formAttributes = {
        name: {
            label: 'Name',
            type: 'text',
            register: register,
            errors: errors,
        },
        email: {
            label: 'Email',
            type: 'email',
            register: register,
            errors: errors,
        },
        password: {
            label: 'Old Password',
            type: 'password',
            register: register,
            errors: errors,
        },
        newPassword: {
            label: 'New Password',
            type: 'password',
            register: register,
            errors: errors,
        },
        role: {
            label: 'Role',
            value: watch('role'),
            options: ['User', 'Admin']
        },
        isTwoFactorEnabled: {
            label: 'Enable Two Factor Authentication',
            enabled: watch('isTwoFactorEnabled') || false
        }
    };

    const onSubmit = handleSubmit(async data => {
        setError(undefined);
        setSuccess(undefined);

        const result = await updateParticulars(data);

        if (!result || result.error) {
            setError(result.error || 'Something went wrong');
            reset();
            return;
        }

        setSuccess(result.success);
        resetField('password');
        resetField('newPassword');
        update();
    });

    return (
        <div className="min-h-full flex justify-center my-10">
            {
                isClient &&
                <div className="flex flex-col justify-center items-center w-1/2 py-10 shadow-md rounded bg-white">
                    <div className="sm:w-full sm:max-w-sm">
                        <h2 className="text-center text-2xl font-bold text-gray-900">
                            Settings
                        </h2>
                    </div>
                    <div className="mt-10 sm:w-full sm:max-w-sm">
                        <form onSubmit={onSubmit} className="flex flex-col gap-3">
                            <div>
                                <InputGroup<TSettingsSchema>
                                    name='name'
                                    {...formAttributes.name}
                                />
                            </div>
                            {
                                user && !user.isOAuth &&
                                <Fragment>
                                    <div>
                                        <InputGroup<TSettingsSchema>
                                            name='email'
                                            {...formAttributes.email}
                                        />
                                    </div>
                                    <div>
                                        <InputGroup<TSettingsSchema>
                                            name='password'
                                            {...formAttributes.password}
                                        />
                                    </div>
                                    <div>
                                        <InputGroup<TSettingsSchema>
                                            name='newPassword'
                                            {...formAttributes.newPassword}
                                        />
                                    </div>
                                </Fragment>
                            }
                            <div>
                                <Controller
                                    name='role'
                                    control={control}
                                    render={({ field: { onChange } }) => (
                                        <SelectDropdown
                                            onChange={(e: any) => {
                                                onChange(e);
                                            }}
                                            {...formAttributes.role}
                                        />
                                    )}
                                />
                            </div>
                            {
                                user && !user.isOAuth &&
                                <div>
                                    <Controller
                                        name='isTwoFactorEnabled'
                                        control={control}
                                        render={({ field: { onChange } }) => (
                                            <Toggle
                                                onChange={(e: any) => {
                                                    onChange(e);
                                                }}
                                                {...formAttributes.isTwoFactorEnabled}
                                            />
                                        )}
                                    />
                                </div>
                            }
                            <button
                                disabled={!isValid || isSubmitting || (!user.isOAuth && !getValues('password'))}
                                type="submit"
                                className="bg-indigo-600 mt-5 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm disabled:bg-slate-300
                                        hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600 rounded"
                            >
                                {isSubmitting ? 'Updating...' : 'Update'}
                            </button>
                        </form>
                    </div>
                    {
                        error && <p>{error}</p>
                    }
                    {
                        success && <p>{success}</p>
                    }
                </div>
            }
        </div>
    );
}
