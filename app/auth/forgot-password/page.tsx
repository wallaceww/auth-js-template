'use client';

import Link from 'next/link';
import { useForm } from 'react-hook-form';
import InputGroup from '../../components/InputGroup';
import { TForgetPasswordSchema, forgetPasswordSchema } from '@/lib/schema';
import { zodResolver } from '@hookform/resolvers/zod';
import { useState } from 'react';
import { forgotPassword } from './action';

export default function ForgotPasswordPage() {
    const {
        register,
        handleSubmit,
        formState: { isSubmitting, isValid, errors },
        reset,
    } = useForm<TForgetPasswordSchema>({
        mode: 'onTouched',
        resolver: zodResolver(forgetPasswordSchema),
    });

    const emailValidation = {
        label: 'Email',
        type: 'text',
        register: register,
        errors: errors,
    };

    const [error, setError] = useState<string | undefined>('');
    const [success, setSuccess] = useState<string | undefined>('');

    const onSubmit = handleSubmit(async data => {
        setError('');
        setSuccess('');

        const result = await forgotPassword(data);

        if (!result) {
            console.log('Something went wrong');
            return;
        }

        if (result.error) {
            setError(result.error);
            return;
        }

        setSuccess(result.success);

        reset();
    });

    return (
        <div className="min-h-full flex justify-center my-10">
            <div className="flex flex-col justify-center items-center w-1/2 py-10 shadow-md rounded bg-white">
                <div className="sm:w-full sm:max-w-sm">
                    <h2 className="text-center text-2xl font-bold text-gray-900">
                        Forgot Password
                    </h2>
                </div>
                <div className="mt-10 sm:w-full sm:max-w-sm">
                    <form onSubmit={onSubmit} className="flex flex-col gap-3">
                        <div>
                            <InputGroup<TForgetPasswordSchema>
                                name='email'
                                placeholder="joe@email.com"
                                {...emailValidation}
                            />
                        </div>
                        <button
                            disabled={!isValid || isSubmitting}
                            type="submit"
                            className="bg-indigo-600 mt-5 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm
                                        disabled:bg-slate-300
                                        hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600 rounded"
                        >
                            {isSubmitting ? 'Submitting...' : 'Submit'}
                        </button>
                    </form>
                    {
                        success && <p>{ success }</p>
                    }
                    {
                        error && <p>{ error }</p>
                    }
                    <div className="mt-5">
                        <p className="text-center">
                            <Link href='/auth/login'
                                className="underline"
                            >
                                Back to login
                            </Link>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
}