'use server';

import { signIn, signOut } from '@/auth';
import { getTwoFactorTokenByToken, getUserByEmail } from '@/lib/dbQuery';
import { Email } from '@/lib/mail';
import { prisma } from '@/lib/prisma';
import { generateTwoFactorToken, generateVerificationToken } from '@/lib/util';
import { AuthError } from 'next-auth';

export async function login(provider?: string) {
    provider ? await signIn(provider) : await signIn();
}

export async function logOut() {
    await signOut();
}

async function sendVerificationEmail(email: string) {
    const { token } = await generateVerificationToken(email);
    const verificationUrl = `http://localhost:3000/auth/new-verification?token=${token}`;
    const verificationEmail = new Email(email, verificationUrl);

    try {
        await verificationEmail.sendVerfication();
        return {
            success: true,
            message: 'Confirmation email sent',
        };
    } catch (error) {
        return {
            success: false,
            message: 'Something went wrong',
        };
    }
}

async function sendTwoFactorCodeEmail(email: string, data: {}) {
    const { token } = await generateTwoFactorToken(email);
    const twoFATokenEmail = new Email(email, token);
    try {
        await twoFATokenEmail.sendTwoFactorCode();
        return {
            success: true,
            message: 'Two factor code sent',
            data,
        };
    } catch (error) {
        return {
            success: false,
            message: 'Something went wrong',
        };
    }
}

export async function authenticate(
    prevState: any,
    formData: FormData
) {
    const email = formData.get('email') as string || prevState?.data?.email;
    const password = formData.get('password') || prevState?.data?.password;
    const code = formData.get('code') as string;

    const existingUser = await getUserByEmail(email);

    if (existingUser && !existingUser.emailVerified) {
        return await sendVerificationEmail(email);
    }

    if (existingUser?.isTwoFactorEnabled) {
        const existingTwoFactorToken = await getTwoFactorTokenByToken(code);

        if (existingTwoFactorToken) {
            await prisma.twoFactorConfirmation.create({
                data: { userId: existingUser.id }
            });

            await prisma.twoFactorToken.delete({
                where: { id: existingTwoFactorToken.id }
            });
        } else {
            return await sendTwoFactorCodeEmail(email, { email, password });
        }
    }

    try {
        await signIn('credentials', {
            email, password, redirectTo: '/',
        });
    } catch (error) {
        if (error instanceof AuthError) {
            if (error.type === 'CredentialsSignin') {
                return {
                    success: false,
                    message: 'Invalid credentials',
                };
            }
            return {
                success: false,
                message: 'Something went wrong',
            };
        }
        throw error;
    }
}
