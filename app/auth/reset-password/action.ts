'use server';

import bcrypt from 'bcrypt';
import { getPasswordResetTokenByToken, getUserByEmail } from '@/lib/dbQuery';
import { TResetPasswordSchema, resetPasswordSchema } from '@/lib/schema';
import { prisma } from '@/lib/prisma';
import moment from 'moment';

export async function verifyResetPasswordToken(token: string) {
    const isTokenValid = await getPasswordResetTokenByToken(token);

    if (!isTokenValid || moment(isTokenValid.expires).isBefore(moment())) {
        return false;
    }

    return { token: isTokenValid };
}

export async function resetPassword(data: TResetPasswordSchema, token: string | null) {
    if(!token) {
        return { error: 'Token is missing' };
    }

    const result = resetPasswordSchema.safeParse(data);

    if (!result.success) {
        return { sucess: false, error: 'Password not valid' };
    }

    const isTokenValid = await verifyResetPasswordToken(token);

    if (!isTokenValid) {
        return { error: 'Token is invalid' };
    }

    const existingUser = await getUserByEmail(isTokenValid.token.email);

    if (!existingUser) {
        return { error: 'Email does not exist' };
    }

    try {
        const hashedPassword = await bcrypt.hash(data.password, 10);
        await prisma.user.update({
            where: {
                id: existingUser.id,
            },
            data: {
                password: hashedPassword,
            }
        });

        await prisma.passwordResetToken.delete({
            where: { id: isTokenValid.token.id },
        });

        return { success: 'Password updated' };
    } catch (error) {
        return { error: 'Something went wrong' };
    }
}