import { prisma } from './prisma';

export async function getUserByEmail(email: string) {
    try {
        return await prisma.user.findUnique({
            where: { email },
        });
    } catch (error) {
        return null;
    }
}

export async function getUserById(id: string) {
    try {
        return await prisma.user.findUnique({
            where: { id },
        });
    } catch (error) {
        return null;
    }
}

export async function getVerificationTokenByEmail(email: string) {
    try {
        return await prisma.verificationToken.findFirst({
            where: { email },
        });
    } catch (error) {
        return null;
    }
}

export async function getVerificationTokenByToken(token: string) {
    try {
        return await prisma.verificationToken.findUnique({
            where: { token },
        });
    } catch (error) {
        return null;
    }
}

export async function getPasswordResetTokenByEmail(email: string) {
    try {
        return await prisma.passwordResetToken.findFirst({
            where: { email },
        });
    } catch (error) {
        return null;
    }
}

export async function getPasswordResetTokenByToken(token: string) {
    try {
        return await prisma.passwordResetToken.findUnique({
            where: { token },
        });
    } catch (error) {
        return null;
    }
}

export async function getTwoFactorTokenByEmail(email: string) {
    try {
        return await prisma.twoFactorToken.findFirst({
            where: { email },
        });
    } catch (error) {
        return null;
    }
}

export async function getTwoFactorTokenByToken(token: string) {
    try {
        return await prisma.twoFactorToken.findUnique({
            where: { token }
        });
    } catch (error) {
        return null;
    }
}

export async function getTwoFactorConfirmationByUserId(userId: string) {
    try {
        return await prisma.twoFactorConfirmation.findUnique({
            where: { userId },
        });
    } catch (error) {
        return null;
    }
}

export async function getAccountByUserId(userId: string) {
    try {
        return await prisma.account.findFirst({
            where: { userId },
        });
    } catch (error) {
        return null;
    }
}
