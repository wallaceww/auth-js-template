import NextAuth from "next-auth";
import { authConfig } from "./auth.config";
import { DEFAULT_LOGIN_REDIRECT, apiRoutePrefix, authRoutes, publicRoutes } from "./lib/routes";

const { auth } = NextAuth(authConfig);

export default auth((req) => {
    const { nextUrl } = req;
    const isLoggedIn = !!req.auth;
    const pathName = nextUrl.pathname;

    const isApiAuthRoute = pathName.startsWith(apiRoutePrefix);
    const isPublicRoute = publicRoutes.includes(pathName);
    const isAuthRoute = authRoutes.includes(pathName);

    if (isApiAuthRoute) {
        return;
    }

    if (isAuthRoute) {
        if (isLoggedIn) {
            return Response.redirect(new URL(DEFAULT_LOGIN_REDIRECT, nextUrl));
        }
        return;
    }

    if (!isLoggedIn && !isPublicRoute) {
        return Response.redirect(new URL('/auth/login', nextUrl));
    }
});

export const config = {
    matcher: ["/((?!.+\\.[\\w]+$|_next).*)", "/", "/(api|trpc)(.*)"],
};
