import { Dialog } from '@headlessui/react';
import { ReactNode } from 'react';

export default function Modal({
    isOpen, onClose, title, children
}: Readonly<{
    isOpen: boolean, onClose: () => void, title?: string, children: ReactNode
}>) {
    return (
        <Dialog open={isOpen} onClose={onClose} className='relative z-50'>
            {/* The backdrop, rendered as a fixed sibling to the panel container */}
            <div className="fixed inset-0 bg-black/30" aria-hidden="true" />

            {/* Full-screen container to center the panel */}
            <div className="fixed inset-0 flex w-screen items-center justify-center p-4">
                <Dialog.Panel className='w-full max-w-sm rounded bg-white'>
                    <Dialog.Title>{title}</Dialog.Title>
                    {children}
                </Dialog.Panel>
            </div>
        </Dialog>
    );
}
