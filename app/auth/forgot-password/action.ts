'use server';

import { getUserByEmail } from '@/lib/dbQuery';
import { Email } from '@/lib/mail';
import { TForgetPasswordSchema, forgetPasswordSchema } from '@/lib/schema';
import { generatePasswordResetToken } from '@/lib/util';

export async function forgotPassword(data: TForgetPasswordSchema) {
    const result = forgetPasswordSchema.safeParse(data);

    if (!result.success) {
        return { sucess: false, error: 'Email not valid' };
    }

    const existingUser = await getUserByEmail(result.data.email);

    if (!existingUser) {
        return { error: 'Email does not exist' };
    }

    const { token } = await generatePasswordResetToken(data.email);

    const resetPasswordUrl = `http://localhost:3000/auth/reset-password?token=${token}`;
    const resetPasswordEmail = new Email(data.email, resetPasswordUrl);
    await resetPasswordEmail.sendPasswordReset();

    return { success: 'Reset email sent' };
}